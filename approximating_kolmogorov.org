#+title: Approximating Kolmogorov complexity
#+author: 
#+date: <2021-12-14 mar>

/Kolmogorov-Chaitin/ complexity is the cornerstone of algorithmic information theory. But its uncomputability restricts heavily its possible applications. Some attempts have been made to approximate it from above, with relative success. Here we present a proof of concept of an approach based on a simple, but high-level, imperative programming language. Its main advantage is that conceptually complex computational tasks are more easily expressed in such a language unlike more lower-level models such as Turing machines or cellular automata. In order to tackle non-halting programs (the main reason behind the uncomputability of Kolmogorov-Chaitin complexity) we applied techniques put forward by Cristian Calude's previous work. We present some promising first results and explain how the project can be escalated.

An image will look like this:

#+CAPTION: This is the caption for the next figure link (or table)
#+NAME:   fig:example figure
[[./images/graf_edu.jpg]]
