build: fake
	@HOME=./.fakehome emacs --batch --load publish.el --funcall org-publish-all

fake:
	@mkdir -p .fakehome
